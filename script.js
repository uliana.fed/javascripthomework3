let num
do {
  num = parseInt(prompt("Введіть число:"));
} while (isNaN(num));

console.log(`Числа, кратні 5, від 0 до ${num}:`);

let found = false;

for (let i = 0; i <= num; i += 5) {
  if (i % 5 === 0) {
    console.log(i);
    found = true;
  }
}

if (!found) {
  console.log("Sorry, no numbers");
}

function getNumber(m, n) {
  if (m >= n || m < 2) {
    console.log("!Error! m повинно бути більше 1, і меньше за n.");
    return;
  }

  const numbers = [];
  for (let i = m; i < n; i++) {
    numbers.push(i);
  }

  const primes = numbers.filter((num) => {
    if (num < 2) return false;
    for (let i = 2; i < num; i++) {
      if (num % i === 0) return false;
    }
    return true;
  });

  console.log(`Прості числа від ${m} до ${n}:`);
  primes.forEach((prime) => console.log(prime));
}

getNumber( 12, 55);